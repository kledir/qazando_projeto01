//var validacao = require('assert');
const { I } = inject()
const npm_cpf = require('../utils/npm_cpf') // esse ../ é pra voltar uma pasta pois estamos dentro da pasta TESTES

Feature('login');

BeforeSuite(() => {
    console.log(npm_cpf.criaCPF())
}) //ANTES DE QUALQUER COISA ele executa o BeforeSuite

Before(() => {
    I.amOnPage('http://automationpratice.com.br/')
}) //executa comandos antes de cada teste, isso vai executar sempre ANTES de CADA CENÁRIO

after(() => {
    console.log('Exemplo de after')
}) //executa comandos antes de cada teste, isso vai executar sempre DEPOIS de CADA CENÁRIO

AfterSuite(() => {
    console.log('Depois de tudo ')
}) //DEPOIS DE QUALQUER COISA ele executa o AfterSuite

Scenario('Teste de Login', async ({ I }) => {
    //pause() //exemplo do comando pause() que possibilita debugar
    I.click('.fa-user');
    I.waitForText('Login',10);
    I.click('#btnLogin')
    
    I.see ('E-mail inválido.')    
}).tag('login');